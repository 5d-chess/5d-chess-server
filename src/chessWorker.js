const { expose } = require('threads/worker');
const Chess = require('5d-chess-js');
const config = require('./config');

const sessionWorker = {
  createSession: (variant = 'standard') => {
    var ses = {
      host: null,
      white: null,
      black: null,
      variant: variant,
      format: null,
      ranked: false,
      ready: false,
      requestJoin: [],
      offerDraw: false,
      started: false,
      startDate: Date.now(),
      ended: false,
      endDate: 0,
      archiveDate: 0,
      processing: false,
      timed: null,
      board: null,
      actionHistory: null,
      moveBuffer: null,
      player: 'white',
      winner: null,
      winCause: null
    };
    var chess = new Chess();
    if(variant.includes('[')) {
      chess.import(variant);
    }
    else {
      chess.reset(variant);
    }
    ses.board = chess.board;
    ses.actionHistory = chess.actionHistory;
    ses.moveBuffer = chess.moveBuffer;
    ses.state = chess.state();
    return ses;
  },
  updateSession: (ses, info = null) => {
    if(typeof info === 'object') {
      if(typeof info.host === 'string') { ses.host = info.host; }
      if(typeof info.white === 'string') { ses.white = info.white; }
      if(typeof info.black === 'string') { ses.black = info.black; }
      if(typeof info.variant === 'string') {
        if(ses.variant !== info.variant) {
          var chess = new Chess();
          if(info.variant.includes('[')) {
            chess.import(info.variant);
          }
          else {
            chess.reset(info.variant);
          }
          ses.board = chess.board;
          ses.actionHistory = chess.actionHistory;
          ses.moveBuffer = chess.moveBuffer;
          ses.state = chess.state();
        }
        ses.variant = info.variant;
      }
      if(typeof info.ranked === 'boolean') { ses.ranked = info.ranked; }
      if(typeof info.timed === 'object') { ses.timed = info.timed; }
      if(typeof info.format === 'string') { ses.format = info.format; }
      if(typeof info.board === 'object') { ses.board = info.board; }
      if(Array.isArray(info.actionHistory)) { ses.actionHistory = info.actionHistory; }
      if(Array.isArray(info.moveBuffer)) { ses.moveBuffer = info.moveBuffer; }
      if(typeof info.player === 'string') { ses.player = info.player; }
    }
    return ses;
  },
  startSession: (ses) => {
    if(ses.ready && !ses.winner && !ses.started) {
      ses.started = true;
      ses.startDate = Date.now();
    }
    else {
      throw 'Non-host player not ready!';
    }
    return ses;
  },
  getSession: (ses) => {
    return ses;
  },
  moveSession: (ses, move) => {
    if(!ses.ended && ses.started) {
      var chess = new Chess();
      chess.state(ses.state);
      chess.move(move);
      ses.board = chess.board;
      ses.actionHistory = chess.actionHistory;
      ses.moveBuffer = chess.moveBuffer;
      ses.player = chess.player;
      ses.state = chess.state();
    }
    return ses;
  },
  undoSession: (ses) => {
    if(!ses.ended && ses.started) {
      var chess = new Chess();
      chess.state(ses.state);
      chess.undo();
      ses.board = chess.board;
      ses.actionHistory = chess.actionHistory;
      ses.moveBuffer = chess.moveBuffer;
      ses.player = chess.player;
      ses.state = chess.state();
    }
    return ses;
  },
  submitSession: (ses) => {
    if(!ses.ended && ses.started) {
      var chess = new Chess();
      chess.checkmateTimeout = 10000;
      chess.state(ses.state);
      chess.submit(true);
      ses.offerDraw = false;
      ses.board = chess.board;
      ses.actionHistory = chess.actionHistory;
      ses.moveBuffer = chess.moveBuffer;
      ses.player = chess.player;
      ses.state = chess.state();
      if(chess.inStalemate) {
        ses.winner = 'draw';
        ses.winCause = 'regular';
        ses.ended = true;
        ses.endDate = Date.now();
        ses.archiveDate = Date.now() + config.archive;
      }
      else if(chess.inCheckmate) {
        ses.winner = chess.player === 'white' ? 'black' : 'white';
        ses.winCause = 'regular';
        ses.ended = true;
        ses.endDate = Date.now();
        ses.archiveDate = Date.now() + config.archive;
      }
    }
    return ses;
  },
  forfeitSession: (ses) => {
    if(!ses.ended && ses.started) {
      ses.winner = ses.player === 'white' ? 'black' : 'white';
      ses.winCause = 'forfeit';
      ses.ended = true;
      ses.endDate = Date.now();
      ses.archiveDate = Date.now() + config.archive;
    }
    return ses;
  },
  drawSession: (ses) => {
    if(!ses.ended && ses.started) {
      ses.winner = 'draw';
      ses.winCause = 'forfeit';
      ses.ended = true;
      ses.endDate = Date.now();
      ses.archiveDate = Date.now() + config.archive;
    }
    return ses;
  },
  exportSession: (ses) => {
    var exportData = {
      white: ses.white,
      black: ses.black,
      variant: ses.variant,
      format: ses.format,
      ranked: ses.ranked,
      startDate: ses.startDate,
      endDate: ses.endDate,
      actionHistory: ses.actionHistory,
      winner: ses.winner,
      winCause: ses.winCause
    };
    return exportData;
  }
};

expose(sessionWorker);
