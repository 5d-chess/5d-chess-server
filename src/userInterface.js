const config = require('./config');
var collections = require('./db').init();
const bcrypt = require('bcrypt');
const { v4: uuidv4 } = require('uuid');
const xmppUser = require('./xmpp/user');
const xmppAvatar = require('./xmpp/avatar');
const emailInterface = require('./emailInterface');
const logInterface = require('./logInterface');
const userRoutes = require('./userRoutes');
const Jimp = require('jimp');
const jdenticon = require('jdenticon');

exports.registerUser = async (username, password, additionalInfo = {}) => {
  var existingUser = await collections.users.findOne({ username: username });
  if(existingUser === null) {
    var newUser = {
      avatar: '',
      username: username,
      passwordHash: (await bcrypt.hash(password, config.saltRounds ? config.saltRounds : 10)),
      fullname: '',
      email: '',
      showEmail: false,
      recoverCode: '',
      recoverTimeout: 0,
      bio: '',
      country: '',
      bot: true,
      joinDate: Date.now(),
      admin: false,
      moderator: false,
      banDate: false,
      xmppPass: '',
    };
    await collections.users.insert(newUser);
    var ret = (await this.updateUser(username, additionalInfo));
    await this.setXmppPass(username, ret);
    if(typeof additionalInfo.avatar !== 'undefined') {
      await this.setAvatar(username, additionalInfo.avatar, ret);
    }
    else {
      await this.setRandomAvatar(username, ret);
    }
    logInterface.addLogEntry('NewUser', 'Info', userRoutes.userTransform(newUser));
    return (await collections.users.findOne({ username: username }));
  }
  else {
    throw 'Username already exists!';
  }
};

exports.authUser = async (username, password) => {
  var existingUser = await collections.users.findOne({ username: username });
  if(existingUser !== null) {
    await this.checkUserBan(username);
    var res = (await bcrypt.compare(password, existingUser.passwordHash));
    if(res) {
      //Clearing recovery code since password works
      if(typeof existingUser.recoverCode !== 'string' || existingUser.recoverCode.length > 0) {
        existingUser.recoverCode = '';
        delete existingUser._id;
        await collections.users.update({ username: username }, { $set: existingUser });
      }
      await this.setXmppPass(username, existingUser);
    }
    return res;
  }
  else {
    throw 'User not found!';
  }
};

exports.recoverUser = async (email, recoverCode, newPassword) => {
  if(config.emailRecovery.enable) {
    var existingUser = await collections.users.findOne({ email: email });
    if(existingUser !== null) {
      if(recoverCode.length <= 0) {
        throw 'No recovery code received!';
      }
      else if(recoverCode !== existingUser.recoverCode) {
        throw 'Recovery code does not match!';
      }
      else if(existingUser.recoverTimeout <= Date.now()) {
        throw 'Recovery process has timed out!';
      }
      else {
        existingUser.passwordHash = (await bcrypt.hash(newPassword, config.saltRounds ? config.saltRounds : 10));
        existingUser.recoverCode = '';
        delete existingUser._id;
        await collections.users.update({ email: email }, { $set: existingUser });
        return existingUser;
      }
    }
    else {
      throw 'User not found!';
    }
  }
  else {
    throw 'Email recovery not enabled!';
  }
};

exports.checkUserBan = async (username) => {
  var existingUser = await collections.users.findOne({ username: username });
  if(existingUser !== null) {
    if(typeof existingUser.banDate === 'boolean' && existingUser.banDate) {
      throw 'User is banned!';
    }
    if(typeof existingUser.banDate === 'number') {
      if(existingUser.banDate > Date.now()) {
        throw 'User is temporarily banned!';
      }
      else {
        existingUser.banDate = false;
        await collections.users.update({ username: username }, { $set: existingUser });
        //Re-enable xmpp user if needed
        if(config.xmpp.enable) {
          try {
            await xmppUser.enableUser(username);
          }
          catch(err) {
            console.error(err);
          }
        }
      }
    }
    return true;
  }
  else {
    throw 'User not found!';
  }
};

exports.banUser = async (username, banDate = true) => {
  let existingUser = await collections.users.findOne({ username: username });
  if(existingUser !== null) {
    if(existingUser.admin) {
      throw 'Cannot ban admin users!';
    }
    existingUser.banDate = banDate;
    await collections.users.update({ username: username }, { $set: existingUser });
    //Disable xmpp user if needed
    if(config.xmpp.enable) {
      try {
        await xmppUser.disableUser(username);
      }
      catch(err) {
        console.error(err);
      }
    }
    return existingUser;
  }
  else {
    throw 'User not found!';
  }
}

exports.updateUser = async (username, additionalInfo = {}) => {
  var existingUser = await collections.users.findOne({ username: username });
  if(existingUser !== null) {
    if(additionalInfo.fullname) { existingUser.fullname = additionalInfo.fullname; }
    if(additionalInfo.email) {
      var existingUser = await collections.users.findOne({ email: additionalInfo.email });
      if(existingUser === null) {
        newUser.email = additionalInfo.email;
      }
      else {
        throw 'User with email already exists!';
      }
    }
    if(additionalInfo.showEmail) { existingUser.showEmail = additionalInfo.showEmail; }
    if(additionalInfo.bio) { existingUser.bio = additionalInfo.bio; }
    if(additionalInfo.country) { existingUser.country = additionalInfo.country; }
    if(typeof additionalInfo.bot !== 'undefined') { existingUser.bot = additionalInfo.bot; }
    if(typeof additionalInfo.admin !== 'undefined') { existingUser.admin = additionalInfo.admin; }
    if(typeof additionalInfo.moderator !== 'undefined') { existingUser.moderator = additionalInfo.moderator; }
    if(typeof additionalInfo.avatar !== 'undefined') {
      await this.setAvatar(username, additionalInfo.avatar, existingUser);
    }
    else {
      await this.setRandomAvatar(username, existingUser);
    }
    delete existingUser._id;
    await collections.users.update({ username: username }, { $set: existingUser });
    logInterface.addLogEntry('UpdateUser', 'Info', userRoutes.userTransform(existingUser));
    return existingUser;
  }
  else {
    throw 'User not found!';
  }
};

exports.updateLastAuth = async (username) => {
  var existingUser = await collections.users.findOne({ username: username });
  if(existingUser !== null) {
    existingUser.lastAuth = Date.now();
    delete existingUser._id;
    await collections.users.update({ username: username }, { $set: existingUser });
    return existingUser;
  }
  else {
    throw 'User not found!';
  }
};

exports.getUser = async (username) => {
  var existingUser = await collections.users.findOne({ username: username });
  if(existingUser !== null) {
    return existingUser;
  }
  else {
    throw 'User not found!';
  }
};

exports.getUsers = async (query = {}, projection = {}, sort = {}, limit = 100, skip = 0) => {
  var res = (await (collections.users.findAsCursor(query, projection).sort(sort).limit(limit).skip(skip)));
  if(!Array.isArray(res)) {
    res = await res.toArray();
  }
  return res;
};

exports.setXmppPass = async (username, existingUser = null) => {
  if(existingUser === null) {
    existingUser = await collections.users.findOne({ username: username });
  }
  //Create xmpp user if not already created
  if(config.xmpp.enable) {
    if(typeof existingUser.xmppPass !== 'string' || (typeof existingUser.xmppPass === 'string' && existingUser.xmppPass.length <= 0)) {
      try {
        var xmppPass = uuidv4();
        await xmppUser.upsertUser(username, xmppPass);
        existingUser.xmppPass = xmppPass;
        await collections.users.update({ username: username }, { $set: existingUser });
      }
      catch(err) {
        console.error(err);
      }
    }
  }
}

exports.setRandomAvatar = async (username, existingUser = null) => {
  if(existingUser === null) {
    existingUser = await collections.users.findOne({ username: username });
  }
  //Add random avatar to user if needed
  if(typeof existingUser.avatar !== 'string' || (typeof existingUser.avatar === 'string' && existingUser.avatar.length <= 0)) {
    try {
      var image = (await Jimp.read(jdenticon.toPng(username, 128)));
      image.rgba(false);
      image.background(0xffffffff);
      image.rgba(true);
      image.filterType(Jimp.PNG_FILTER_AUTO);
      image.deflateLevel(9);
      var dataUrl = (await image.getBase64Async(Jimp.MIME_PNG));
      xmppAvatar.upsertAvatar(username, dataUrl, existingUser);
      existingUser.avatar = dataUrl;
      await collections.users.update({ username: username }, { $set: existingUser });
    }
    catch(err) {
      console.error(err);
    }
  }
}

exports.setAvatar = async (username, data, existingUser = null) => {
  if(existingUser === null) {
    existingUser = await collections.users.findOne({ username: username });
  }
  //Add avatar to user
  try {
    if(data.includes('data:image')) {
      data = Buffer.from(data.split(',')[1], 'base64');
    }
    var image = (await Jimp.read(data));
    image.cover(128, 128, Jimp.HORIZONTAL_ALIGN_CENTER | Jimp.VERTICAL_ALIGN_MIDDLE);
    image.rgba(false);
    image.background(0xffffffff);
    image.rgba(true);
    image.filterType(Jimp.PNG_FILTER_AUTO);
    image.deflateLevel(9);
    var dataUrl = (await image.getBase64Async(Jimp.MIME_PNG));
    xmppAvatar.upsertAvatar(username, dataUrl, existingUser);
    existingUser.avatar = dataUrl;
    await collections.users.update({ username: username }, { $set: existingUser });
  }
  catch(err) {
    console.error(err);
  }
}

exports.setRecoverCode = async (email) => {
  //Set recovery code if email recovery is available
  if(config.emailRecovery.enable) {
    var existingUser = await collections.users.findOne({ email: email });
    if(existingUser !== null) {
      var newRecoverCode = uuidv4().slice(0, 8);
      existingUser.recoverCode = newRecoverCode;
      existingUser.recoverTimeout = Date.now() + config.emailRecovery.timeout;
      delete existingUser._id;
      await collections.users.update({ username: existingUser.username }, { $set: existingUser });
      await emailInterface.sendRecoveryCode(email, newRecoverCode);
      return existingUser;
    }
    else {
      throw 'User not found!';
    }
  }
  else {
    throw 'Email recovery not enabled!';
  }
}