const config = require('../config');
const { client, xml } = require("@xmpp/client");
const sha1 = require('sha1');
const initXmpp = require('./xmpp');
var collections = require('../db').init();

exports.upsertAvatar = async (username, avatarDataUrl, existingUser = null) => {
  if(!config.xmpp.enable) { return null; }
  if(config.usernameBlacklist.includes(username)) { return null; }
  var xmpp = (await initXmpp.init());

  if(existingUser === null) {
    existingUser = await collections.users.findOne({ username: username });
  }
  if(existingUser !== null) {
    if(typeof existingUser.xmppPass === 'string' && existingUser.xmppPass.length > 0) {
      var xmpp = new client({
        service: config.xmpp.service,
        domain: config.xmpp.domain,
        username: username,
        password: existingUser.xmppPass,
      });
      //Initialize user xmpp connection
      await (new Promise((resolve) => {
        if(xmpp.status === 'online') {
          resolve(xmpp);
        }
        else {
          xmpp.once('online', async () => {
            await xmpp.send(xml('presence'));
            resolve(xmpp);
          });
          if(
            xmpp.status === 'offline' ||
            xmpp.status === 'closing' ||
            xmpp.status === 'close' ||
            xmpp.status === 'disconnecting' ||
            xmpp.status === 'disconnect'
          ) {
            xmpp.start();
          }
        }
      }));
      //Send data publishing avatar
      var data = avatarDataUrl.replace('data:image/png;base64,','');
      var hash = sha1(data);
      await (new Promise((resolve, reject) => {
        var requestId = `${Date.now()}`;
        var requestMessage = xml('iq', 
          {
            id: requestId,
            from: `${username}@${config.xmpp.domain}`,
            type: 'set'
          }, 
          xml('pubsub', 
            { xmlns: 'http://jabber.org/protocol/pubsub' },
            xml('publish',
              { node: 'urn:xmpp:avatar:data' },
              xml('item',
                { id: hash },
                xml('data',
                  { xmlns: 'urn:xmpp:avatar:data' },
                  data
                )
              )
            )
          )
        );
        xmpp.on('stanza', (result) => {
          if(result.attrs.id === requestId) {
            if(result.attrs.type === 'result') {
              xmpp.removeListener('stanza', arguments.callee);
              resolve();
            }
            else {
              console.error(result);
              reject(new Error('XMPP Server reports error'));
            }
          }
        });
        xmpp.send(requestMessage);
      }));
      //Send data publishing avatar metadata
      await (new Promise((resolve, reject) => {
        var requestId = `${Date.now()}`;
        var requestMessage = xml('iq', 
          {
            id: requestId,
            from: `${username}@${config.xmpp.domain}`,
            type: 'set'
          }, 
          xml('pubsub', 
            { xmlns: 'http://jabber.org/protocol/pubsub' },
            xml('publish',
              { node: 'urn:xmpp:avatar:metadata' },
              xml('item',
                { id: hash },
                xml('metadata',
                  { xmlns: 'urn:xmpp:avatar:metadata' },
                  xml('info',
                    {
                      id: hash,
                      height: '128',
                      width: '128',
                      type: 'image/png'
                    }
                  )
                )
              )
            )
          )
        );
        xmpp.on('stanza', (result) => {
          if(result.attrs.id === requestId) {
            if(result.attrs.type === 'result') {
              xmpp.removeListener('stanza', arguments.callee);
              resolve();
            }
            else {
              console.error(result);
              reject(new Error('XMPP Server reports error'));
            }
          }
        });
        xmpp.send(requestMessage);
      }));
    }
  }
}