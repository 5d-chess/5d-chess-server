const config = require('../config');
const { xml, jid } = require("@xmpp/client");
const initXmpp = require('./xmpp');

exports.addRoom = async (room) => {
  if(!config.xmpp.enable) { return null; }
  var xmpp = (await initXmpp.init());
  
  //Initial add room request
  var requestMessage = xml('presence', 
    {
      to: `${room}@${config.xmpp.muc}/${config.xmpp.username}`
    }, 
    xml('x', 
      {
        xmlns: 'http://jabber.org/protocol/muc'
      }
    )
  );
  await xmpp.send(requestMessage);
  //Instant room creation request
  var requestId = `${Date.now()}`;
  var requestMessage = xml('iq', 
    {
      id: requestId,
      to: `${room}@${config.xmpp.muc}`,
      type: 'set'
    }, 
    xml('query', 
      {
        xmlns: 'http://jabber.org/protocol/muc#owner'
      },
      xml('x',
        {
          xmlns: 'jabber:x:data',
          type: 'submit'
        }
      )
    )
  );
  await xmpp.send(requestMessage);
}

exports.deleteRoom = async (room, reason = 'Room destroyed by server') => {
  if(!config.xmpp.enable) { return null; }
  var xmpp = (await initXmpp.init());
  
  //Destroy room request
  var requestId = `${Date.now()}`;
  var requestMessage = xml('iq', 
    {
      id: requestId,
      to: `${room}@${config.xmpp.muc}`,
      type: 'set'
    }, 
    xml('query', 
      {
        xmlns: 'http://jabber.org/protocol/muc#owner'
      },
      xml('destroy', null,
        xml('reason', null, reason)
      )
    )
  );
  await xmpp.send(requestMessage);
}