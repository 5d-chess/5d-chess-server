const logInterface = require('./logInterface');
const userInterface = require('./userInterface');

exports.getInfoQuery = async (req, res) => {
  let currentUser = await userInterface.getUser(req.username);
  if(currentUser.moderator || currentUser.admin) {
    try {
      if(typeof req.body !== 'object') {
        req.body = {
          query: {},
          projection: {},
          sort: {},
          limit: 100,
          skip: 0
        };
      }
      if(typeof req.body.query !== 'object') { req.body.query = {}; }
      if(typeof req.body.projection !== 'object') { req.body.projection = {}; }
      if(typeof req.body.sort !== 'object') { req.body.sort = {}; }
      if(typeof req.body.limit !== 'number') { req.body.limit = 100; }
      if(typeof req.body.skip !== 'number') { req.body.skip = 0; }
      let logEntries = (await logInterface.getLogEntries(req.body.query, req.body.projection, req.body.sort, req.body.limit, req.body.skip));
      res.status(200).send(logEntries);
    }
    catch(err) {
      res.status(500).send({ error: typeof err === 'string' ? err : err.message });
    }
  }
  else {
    res.status(403).send({ error: 'Requesting user is not moderator or admin!' });
  }
};
