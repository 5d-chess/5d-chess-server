const config = require('./config');
const collections = require('./db').init();

exports.addLogEntry = async (type, logLevel, message) => {
  await collections.logs.insert({
    date: Date.now(),
    type: type,
    logLevel: logLevel,
    message: message
  });
};

exports.cleanLog = async () => {
  await collections.logs.remove({ date: { $lte: Date.now() - config.maxLogTime } });
};

exports.getLogEntries = async (query = {}, projection = {}, sort = {}, limit = 100, skip = 0) => {
  let res = (await (collections.logs.findAsCursor(query, projection).sort(sort).limit(limit).skip(skip)));
  if(!Array.isArray(res)) {
    res = await res.toArray();
  }
  return res;
};