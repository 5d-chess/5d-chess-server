const secret = require('./secret');
const jwt = require('jsonwebtoken');
const userInterface = require('./userInterface');

exports.tokenSign = async (username) => {
  try {
    return jwt.sign({ username: username }, await secret.getSecret(), {
      expiresIn: 12 * 60 * 60
    });
  }
  catch(err) {
    throw err;
  }
};

exports.tokenVerify = async (token) => {
  try {
    return jwt.verify(token, await secret.getSecret(), {
      maxAge: 12 * 60 * 60
    });
  }
  catch(err) {
    throw err;
  }
};

exports.authRoute = async (req, res, next) => {
  req.username = null;
  if(!req.headers.authorization) {
    res.status(403).send({ error: 'No credentials sent!' });
  }
  else {
    try {
      req.username = (await this.tokenVerify(req.headers.authorization)).username;
      await userInterface.updateLastAuth(req.username);
      await userInterface.checkUserBan(req.username);
      next();
    }
    catch(err) {
      res.status(401).send(err);
    }
  }
};

exports.softAuthRoute = async (req, res, next) => {
  if(req.headers.authorization) {
    try {
      await userInterface.updateLastAuth((await this.tokenVerify(req.headers.authorization)).username);
    }
    catch(err) {}
  }
  next();
};
