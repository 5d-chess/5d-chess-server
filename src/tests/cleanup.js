const fs = require('fs');

module.exports = async (globalConfig, projectConfig) => {
  try {
    fs.rmSync('./config.json', { recursive: true });
  }
  catch(err) {}
  try {
    fs.renameSync('./tmpConfig.json', './config.json');
  }
  catch(err) {}
}