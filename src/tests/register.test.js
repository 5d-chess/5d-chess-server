const app = require('../app');
const supertest = require('supertest');
const request = supertest(app);
const testSetup = require('./testSetup');
beforeAll(async () => { await testSetup.register(request); });

test('Register multiple users', async () => {
  var res1 = await request.post('/register').send({
    username: 'registeruser1',
    password: 'registeruser1'
  });
  expect(res1.status).toBe(200);
  expect(typeof res1.text).toBe('string');

  var res2 = await request.post('/register').send({
    username: 'registeruser2',
    password: 'registeruser2',
    bio: 'Test biography'
  });
  expect(res2.status).toBe(200);
  expect(typeof res2.text).toBe('string');

  var res3 = await request.post('/register').send({
    username: 'registeruser3',
    password: 'registeruser3',
    bio: 'Test biography',
    fullname: 'Test User 3',
    country: 'USA'
  });
  expect(res3.status).toBe(200);
  expect(typeof res3.text).toBe('string');
});

test('Login multiple users', async () => {
  var res1 = await request.post('/login').send({
    username: 'registeruser1',
    password: 'registeruser1'
  });
  expect(res1.status).toBe(200);
  expect(typeof res1.text).toBe('string');

  var res2 = await request.post('/login').send({
    username: 'registeruser2',
    password: 'registeruser2'
  });
  expect(res2.status).toBe(200);
  expect(typeof res2.text).toBe('string');

  var res3 = await request.post('/login').send({
    username: 'registeruser3',
    password: 'registeruser3'
  });
  expect(res3.status).toBe(200);
  expect(typeof res3.text).toBe('string');
});

test('Register invalid users', async () => {
  var res4 = await request.post('/register').send({
    username: 'registeruser4',
    password: 'registeruser4'
  });
  expect(res4.status).toBe(200);
  expect(typeof res4.text).toBe('string');

  var res5 = await request.post('/register').send({
    username: 'registeruser4',
    password: 'registeruser4'
  });
  expect(res5.status).toBe(403);
  expect(res5.body).toStrictEqual({
    error: 'Username already exists!'
  });

  var res6 = await request.post('/register').send({
    username: 'registeruser5',
    password: 'registeruser5',
    country: 'US'
  });
  expect(res6.status).toBe(403);
  expect(res6.body).toStrictEqual({
    error: 'Country field is not ISO 3166-1 Alpha-3 compliant! (List here: https://en.wikipedia.org/wiki/ISO_3166-1_alpha-3#Officially_assigned_code_elements)'
  });
});

test('Login invalid users', async () => {
  var res4 = await request.post('/login').send({
    username: 'registeruser1',
    password: 'wrongpassword1'
  });
  expect(res4.status).toBe(403);
  expect(res4.body).toStrictEqual({
    error: 'Username or Password do not match!'
  });

  var res5 = await request.post('/login').send({
    username: 'registeruser2',
    password: 'wrongpassword2'
  });
  expect(res5.status).toBe(403);
  expect(res5.body).toStrictEqual({
    error: 'Username or Password do not match!'
  });
});
