const app = require('../app');
const supertest = require('supertest');
const request = supertest(app);
const testSetup = require('./testSetup');
beforeAll(async () => { await testSetup.register(request); });

test('Test elevating user to admin', async () => {
  let admin = await request.post('/login').send({
    username: 'admin',
    password: await testSetup.getAdminPass()
  });
  expect(admin.status).toBe(200);
  expect(typeof admin.text).toBe('string');
  let adminToken = admin.text;

  let user1 = await request.post('/login').send({
    username: 'user1',
    password: 'user1'
  });
  expect(user1.status).toBe(200);
  expect(typeof user1.text).toBe('string');
  let user1Token = user1.text;

  let res1 = await request.post('/users/user1/admin').set('Authorization', adminToken).send({
    admin: false
  });
  expect(res1.status).toBe(200);

  let res2 = await request.post('/users/user2/ban').set('Authorization', user1Token).send({
    banDate: false
  });
  expect(res2.status).toBe(403);
  expect(res2.body).toStrictEqual({
    error: 'Requesting user is not moderator or admin!'
  });

  let res3 = await request.post('/users/user1/admin').set('Authorization', adminToken).send({
    admin: true
  });
  expect(res3.status).toBe(200);

  let res4 = await request.post('/users/user2/ban').set('Authorization', user1Token).send({
    banDate: false
  });
  expect(res4.status).toBe(200);

  let res5 = await request.post('/users/user1/admin').set('Authorization', adminToken).send({
    admin: false
  });
  expect(res5.status).toBe(200);
});

test('Test elevating user to admin from unauthorized user', async () => {
  let admin = await request.post('/login').send({
    username: 'admin',
    password: await testSetup.getAdminPass()
  });
  expect(admin.status).toBe(200);
  expect(typeof admin.text).toBe('string');
  let adminToken = admin.text;

  let user1 = await request.post('/login').send({
    username: 'user1',
    password: 'user1'
  });
  expect(user1.status).toBe(200);
  expect(typeof user1.text).toBe('string');
  let user1Token = user1.text;

  let res1 = await request.post('/users/user1/admin').set('Authorization', adminToken).send({
    admin: false
  });
  expect(res1.status).toBe(200);

  let res2 = await request.post('/users/user2/admin').set('Authorization', user1Token).send({
    admin: false
  });
  expect(res2.status).toBe(403);
  expect(res2.body).toStrictEqual({
    error: 'Requesting user is not admin!'
  });

  let res3 = await request.post('/users/user2/admin').set('Authorization', user1Token).send({
    admin: true
  });
  expect(res3.status).toBe(403);
  expect(res3.body).toStrictEqual({
    error: 'Requesting user is not admin!'
  });
});