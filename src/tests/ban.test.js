const app = require('../app');
const supertest = require('supertest');
const request = supertest(app);
const testSetup = require('./testSetup');
beforeAll(async () => { await testSetup.register(request); });

test('Test indefinite user ban', async () => {
  let admin = await request.post('/login').send({
    username: 'admin',
    password: await testSetup.getAdminPass()
  });
  expect(admin.status).toBe(200);
  expect(typeof admin.text).toBe('string');
  let adminToken = admin.text;

  let res1 = await request.post('/users/user2/ban').set('Authorization', adminToken).send({
    banDate: false
  });
  expect(res1.status).toBe(200);

  let res2 = await request.post('/login').send({
    username: 'user2',
    password: 'user2'
  });
  expect(res2.status).toBe(200);
  expect(typeof res2.text).toBe('string');

  let res3 = await request.post('/users/user2/ban').set('Authorization', adminToken).send({
    banDate: true
  });
  expect(res3.status).toBe(200);

  let res4 = await request.post('/login').send({
    username: 'user2',
    password: 'user2'
  });
  expect(res4.status).toBe(403);
  expect(res4.body).toStrictEqual({
    error: 'User is banned!'
  });
});

test('Test temporary user ban', async () => {
  let admin = await request.post('/login').send({
    username: 'admin',
    password: await testSetup.getAdminPass()
  });
  expect(admin.status).toBe(200);
  expect(typeof admin.text).toBe('string');
  let adminToken = admin.text;

  let res1 = await request.post('/users/user2/ban').set('Authorization', adminToken).send({
    banDate: false
  });
  expect(res1.status).toBe(200);

  let res2 = await request.post('/login').send({
    username: 'user2',
    password: 'user2'
  });
  expect(res2.status).toBe(200);
  expect(typeof res2.text).toBe('string');

  let res3 = await request.post('/users/user2/ban').set('Authorization', adminToken).send({
    banDate: Date.now() + 2500
  });
  expect(res3.status).toBe(200);

  let res4 = await request.post('/login').send({
    username: 'user2',
    password: 'user2'
  });
  expect(res4.status).toBe(403);
  expect(res4.body).toStrictEqual({
    error: 'User is temporarily banned!'
  });

  //Wait 5 seconds
  await new Promise((r) => { setTimeout(r, 5000); });

  let res5 = await request.post('/login').send({
    username: 'user2',
    password: 'user2'
  });
  expect(res5.status).toBe(200);
  expect(typeof res5.text).toBe('string');
});

test('Test user ban from unauthorized user', async () => {
  let user1 = await request.post('/login').send({
    username: 'user1',
    password: 'user1'
  });
  expect(user1.status).toBe(200);
  expect(typeof user1.text).toBe('string');
  let user1Token = user1.text;

  let res1 = await request.post('/users/user2/ban').set('Authorization', user1Token).send({
    banDate: false
  });
  expect(res1.status).toBe(403);
  expect(res1.body).toStrictEqual({
    error: 'Requesting user is not moderator or admin!'
  });

  let res2 = await request.post('/users/user2/ban').set('Authorization', user1Token).send({
    banDate: true
  });
  expect(res2.status).toBe(403);
  expect(res2.body).toStrictEqual({
    error: 'Requesting user is not moderator or admin!'
  });

  let res3 = await request.post('/users/user2/ban').set('Authorization', user1Token).send({
    banDate: Date.now()
  });
  expect(res3.status).toBe(403);
  expect(res3.body).toStrictEqual({
    error: 'Requesting user is not moderator or admin!'
  });
});