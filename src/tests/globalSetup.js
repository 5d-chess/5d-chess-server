const fs = require('fs');

module.exports = async (globalConfig, projectConfig) => {
  try {
    fs.rmdirSync('./db', { recursive: true });
  }
  catch(err) {}
  try {
    fs.renameSync('./config.json', './tmpConfig.json');
  }
  catch(err) {}
  try {
    fs.copyFileSync('./defaultConfig.json', './config.json');
    let currConfig = JSON.parse(fs.readFileSync('./config.json'));
    currConfig.db = {
      type: 'memory'
    };
    fs.writeFileSync('./config.json', JSON.stringify(currConfig))
  }
  catch(err) {}
}