const deepmerge = require('deepmerge');

var config = null;

try {
  var newConfig = require('../config.json');
  var defaultConfig = require('../defaultConfig.json');
  config = deepmerge(defaultConfig, newConfig);
}
catch(err) {
  config = require('../defaultConfig.json');
}

module.exports = config;
