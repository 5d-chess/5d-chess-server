const config = require('./config');
const Datastore = require('nedb-promises');
const mongoist = require('mongoist');
const fs = require('fs');

var collections = null;

exports.init = () => {
  if(collections === null) {
    if(!config.db || config.db === null) {
      try { fs.mkdirSync('./db'); } catch(err) {}
      collections = {
        users: Datastore.create({ filename: './db/users.nedb', autoload: true }),
        games: Datastore.create({ filename: './db/games.nedb', autoload: true }),
        quickplay: Datastore.create({ filename: './db/quickplay.nedb', autoload: true }),
        sessions: Datastore.create({ filename: './db/sessions.nedb', autoload: true }),
        rankings: Datastore.create({ filename: './db/rankings.nedb', autoload: true }),
        secrets: Datastore.create({ filename: './db/secrets.nedb', autoload: true }),
        logs: Datastore.create({ filename: './db/logs.nedb', autoload: true })
      };
      collections.users.ensureIndex({ fieldName: 'username' });
      collections.games.ensureIndex({ fieldName: 'id' });
      collections.quickplay.ensureIndex({ fieldName: 'id' });
      collections.sessions.ensureIndex({ fieldName: 'id' });
      collections.rankings.ensureIndex({ fieldName: 'id' });
      collections.logs.ensureIndex({ fieldName: 'date' });
      collections.users.findAsCursor = collections.users.find;
      collections.games.findAsCursor = collections.games.find;
      collections.quickplay.findAsCursor = collections.quickplay.find;
      collections.sessions.findAsCursor = collections.sessions.find;
      collections.rankings.findAsCursor = collections.rankings.find;
      collections.secrets.findAsCursor = collections.secrets.find;
      collections.logs.findAsCursor = collections.logs.find;
    }
    else {
      if(config.db.type === 'memory') {
        collections = {
          users: Datastore.create({ inMemoryOnly: true }),
          games: Datastore.create({ inMemoryOnly: true }),
          quickplay: Datastore.create({ inMemoryOnly: true }),
          sessions: Datastore.create({ inMemoryOnly: true }),
          rankings: Datastore.create({ inMemoryOnly: true }),
          secrets: Datastore.create({ inMemoryOnly: true }),
          logs: Datastore.create({ inMemoryOnly: true })
        };
        collections.users.ensureIndex({ fieldName: 'username' });
        collections.games.ensureIndex({ fieldName: 'id' });
        collections.quickplay.ensureIndex({ fieldName: 'id' });
        collections.sessions.ensureIndex({ fieldName: 'id' });
        collections.rankings.ensureIndex({ fieldName: 'id' });
        collections.logs.ensureIndex({ fieldName: 'date' });
        collections.users.findAsCursor = collections.users.find;
        collections.games.findAsCursor = collections.games.find;
        collections.quickplay.findAsCursor = collections.quickplay.find;
        collections.sessions.findAsCursor = collections.sessions.find;
        collections.rankings.findAsCursor = collections.rankings.find;
        collections.secrets.findAsCursor = collections.secrets.find;
        collections.logs.findAsCursor = collections.logs.find;
      }
      if(config.db.type === 'mongodb') {
        var db = {};
        if(config.db.options) {
          db = mongoist(config.db.url, config.db.options);
        }
        else {
          db = mongoist(config.db.url);
        }
        collections = {
          users: db.users,
          games: db.games,
          quickplay: db.quickplay,
          sessions: db.sessions,
          rankings: db.rankings,
          secrets: db.secrets,
          logs: db.logs
        };
        collections.users.createIndex({ username: 1 });
        collections.games.createIndex({ id: 1 });
        collections.quickplay.createIndex({ id: 1 });
        collections.sessions.createIndex({ id: 1 });
        collections.rankings.createIndex({ id: 1 });
        collections.logs.createIndex({ date: 1 });
      }
    }
  }
  return collections;
};
