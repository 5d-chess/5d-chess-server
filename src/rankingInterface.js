const config = require('./config');
var collections = require('./db').init();
const { v5: uuidv5 } = require('uuid');

const initRanking = async (username, variant, format) => {
  var existingUser = await collections.users.findOne({ username: username });
  if(existingUser !== null) {
    var initRanking = await collections.rankings.findOne({ username: username, variant: variant, format: format });
    if(initRanking === null) {
      let startingElo = config.startingElo;
      //Check if overall starting elo exists already
      if(variant !== 'overall' && format !== 'overall') {
        let overallInitRanking = await collections.rankings.findOne({ username: username, variant: 'overall', format: 'overall', date: existingUser.joinDate });
        if(overallInitRanking !== null) {
          startingElo = overallInitRanking.rating;
        }
      }
      await collections.rankings.insert({
        id: uuidv5(username + startingElo + variant + format + existingUser.joinDate, '214fcb11-72b6-4d41-ab2e-797fc4ccae07'),
        username: username,
        rating: startingElo,
        variant: variant,
        format: format,
        date: existingUser.joinDate,
        gameId: ''
      });
    }
  }
  else {
    throw 'User not found!';
  }
};

exports.addRanking = async (username, rating, variant, format, gameId, date = null) => {
  var existingUser = await collections.users.findOne({ username: username });
  if(existingUser !== null) {
    await initRanking(username, variant, format);
    var newDate = Date.now();
    var newRanking = {
      id: uuidv5(username + rating + variant + format + (date ? date : newDate), '214fcb11-72b6-4d41-ab2e-797fc4ccae07'),
      username: username,
      rating: rating,
      variant: variant,
      format: format,
      date: date ? date : newDate,
      gameId: gameId
    };
    return (await collections.rankings.insert(newRanking));
  }
  else {
    throw 'User not found!';
  }
};

exports.getLatestRanking = async (username, variant, format) => {
  var existingUser = await collections.users.findOne({ username: username });
  if(existingUser !== null) {
    await initRanking(username, variant, format);
    var rankings = await collections.rankings.findAsCursor({ username: username, variant: variant, format: format }).sort({ date: -1 }).limit(1);
    return rankings[0];
  }
  else {
    throw 'User not found!';
  }
};

exports.getRanking = async (id) => {
  var existingRanking = await collections.rankings.findOne({ id: id });
  if(existingRanking !== null) {
    return existingRanking;
  }
  else {
    throw 'Ranking not found!';
  }
};

exports.getRankings = async (query = {}, projection = {}, sort = {}, limit = 100, skip = 0) => {
  var res = (await (collections.rankings.findAsCursor(query, projection).sort(sort).limit(limit).skip(skip)));
  if(!Array.isArray(res)) {
    res = await res.toArray();
  }
  return res;
};
