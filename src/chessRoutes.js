const chessInterface = require('./chessInterface');
const userInterface = require('./userInterface');

const sessionTransform = (ses) => {
  return {
    id: ses.id,
    host: ses.host,
    white: ses.white,
    black: ses.black,
    variant: ses.variant,
    format: ses.format,
    ranked: ses.ranked,
    ready: ses.ready,
    requestJoin: ses.requestJoin,
    offerDraw: ses.offerDraw,
    started: ses.started,
    startDate: ses.startDate,
    ended: ses.ended,
    endDate: ses.endDate,
    processing: ses.processing,
    archiveDate: ses.archiveDate,
    timed: ses.timed,
    board: ses.board,
    actionHistory: ses.actionHistory,
    moveBuffer: ses.moveBuffer,
    player: ses.player,
    winner: ses.winner,
    winCause: ses.winCause
  };
};

exports.new = async (req, res) => {
  var data = req.body;
  var format = undefined;
  var valid = true;
  var error = '';
  if(typeof data.format === 'string') {
    format = data.format;
  }
  if(valid && typeof data.player === 'string') {
    if(data.player !== 'white' && data.player !== 'black' && data.player !== 'random') {
      error = 'Player field is not valid! Must be \'white\', \'black\', or \'random\'.';
      valid = false;
    }
  }
  if(valid && typeof data.variant !== 'string') {
    data.variant = 'standard';
  }
  if(valid && typeof data.ranked !== 'undefined') {
    if(typeof data.ranked !== 'boolean') {
      error = 'Ranked field is not boolean!';
      valid = false;
    }
    else if(data.ranked) {
      try {
        rankedFuncs.validRanked(data.variant, data.format);
      }
      catch(err) {
        error = typeof err === 'string' ? err : err.message;
        valid = false;
      }
    }
  }
  if(valid) {
    try {
      var newSession = await chessInterface.newSession(req.username, data.player, data.variant, data.ranked, format);
      res.status(200).send(sessionTransform(newSession));
    }
    catch(err) {
      error = typeof err === 'string' ? err : err.message;
      valid = false;
    }
  }
  if(!valid) {
    if(!res.headersSent) { res.status(403).send({ error: error }); }
  }
};

exports.update = async (req, res) => {
  var data = req.body;
  var format = undefined;
  var valid = true;
  var error = '';
  if(typeof req.params.id !== 'string') {
    valid = false;
    error = 'Session id parameter is not a string!';
  }
  if(valid && !(await chessInterface.authHost(req.params.id, req.username))) {
    valid = false;
    res.status(403).send({ error: 'User is not host!' });
  }
  if(valid && typeof data.format === 'string') {
    format = data.format;
  }
  if(valid && typeof data.player === 'string') {
    if(data.player !== 'white' && data.player !== 'black') {
      error = 'Player field is not valid! Must be \'white\' or \'black\'.';
      valid = false;
    }
  }
  if(valid && typeof data.variant !== 'string') {
    data.variant = 'standard';
  }
  if(valid && typeof data.ranked !== 'undefined') {
    if(typeof data.ranked !== 'boolean') {
      error = 'Ranked field is not boolean!';
      valid = false;
    }
  }
  if(valid) {
    try {
      var existingSession = await chessInterface.updateSession(req.params.id, data.player, data.variant, data.ranked, timed);
      res.status(200).send(sessionTransform(existingSession));
    }
    catch(err) {
      error = typeof err === 'string' ? err : err.message;
      valid = false;
    }
  }
  if(!valid) {
    if(!res.headersSent) { res.status(403).send({ error: error }); }
  }
};

exports.remove = async (req, res) => {
  var data = req.body;
  var valid = true;
  var error = '';
  if(typeof req.params.id !== 'string') {
    valid = false;
    error = 'Session id parameter is not a string!';
  }
  if(valid && !(await chessInterface.authHost(req.params.id, req.username))) {
    valid = false;
    res.status(403).send({ error: 'User is not host!' });
  }
  if(valid) {
    try {
      await chessInterface.removeSession(req.params.id);
      res.status(200).end();
    }
    catch(err) {
      error = typeof err === 'string' ? err : err.message;
      valid = false;
    }
  }
  if(!valid) {
    if(!res.headersSent) { res.status(403).send({ error: error }); }
  }
};

exports.addUser = async (req, res) => {
  var data = req.body;
  var valid = true;
  var error = '';
  if(typeof req.params.id !== 'string') {
    valid = false;
    error = 'Session id parameter is not a string!';
  }
  if(valid && typeof data.username === 'string') {
    try {
      await userInterface.getUser(data.username);
    }
    catch(err) {
      error = typeof err === 'string' ? err : err.message;
      valid = false;
    }
  }
  else {
    valid = false;
    error = 'Username parameter is not a string!';
  }
  if(valid && !(await chessInterface.authHost(req.params.id, req.username))) {
    valid = false;
    res.status(403).send({ error: 'User is not host!' });
  }
  if(valid) {
    try {
      var existingSession = await chessInterface.addUser(req.params.id, data.username);
      res.status(200).send(sessionTransform(existingSession));
    }
    catch(err) {
      error = typeof err === 'string' ? err : err.message;
      valid = false;
    }
  }
  if(!valid) {
    if(!res.headersSent) { res.status(403).send({ error: error }); }
  }
};

exports.requestJoin = async (req, res) => {
  var data = req.body;
  var valid = true;
  var error = '';
  if(typeof req.params.id !== 'string') {
    valid = false;
    error = 'Session id parameter is not a string!';
  }
  if(valid) {
    try {
      var existingSession = await chessInterface.requestJoin(req.params.id, req.username);
      res.status(200).send(sessionTransform(existingSession));
    }
    catch(err) {
      error = typeof err === 'string' ? err : err.message;
      valid = false;
    }
  }
  if(!valid) {
    if(!res.headersSent) { res.status(403).send({ error: error }); }
  }
};

exports.ready = async (req, res) => {
  var data = req.body;
  var valid = true;
  var error = '';
  if(typeof req.params.id !== 'string') {
    valid = false;
    error = 'Session id parameter is not a string!';
  }
  if(valid && !(await chessInterface.authNonHost(req.params.id, req.username))) {
    valid = false;
    res.status(403).send({ error: 'User must be non-host player!' });
  }
  if(valid) {
    try {
      var existingSession = await chessInterface.sessionReady(req.params.id);
      res.status(200).send(sessionTransform(existingSession));
    }
    catch(err) {
      error = typeof err === 'string' ? err : err.message;
      valid = false;
    }
  }
  if(!valid) {
    if(!res.headersSent) { res.status(403).send({ error: error }); }
  }
};

exports.unready = async (req, res) => {
  var data = req.body;
  var valid = true;
  var error = '';
  if(typeof req.params.id !== 'string') {
    valid = false;
    error = 'Session id parameter is not a string!';
  }
  if(valid && !(await chessInterface.authNonHost(req.params.id, req.username))) {
    valid = false;
    res.status(403).send({ error: 'User must be non-host player!' });
  }
  if(valid) {
    try {
      var existingSession = await chessInterface.sessionUnready(req.params.id);
      res.status(200).send(sessionTransform(existingSession));
    }
    catch(err) {
      error = typeof err === 'string' ? err : err.message;
      valid = false;
    }
  }
  if(!valid) {
    if(!res.headersSent) { res.status(403).send({ error: error }); }
  }
};

exports.start = async (req, res) => {
  var data = req.body;
  var valid = true;
  var error = '';
  if(typeof req.params.id !== 'string') {
    valid = false;
    error = 'Session id parameter is not a string!';
  }
  if(valid && !(await chessInterface.authHost(req.params.id, req.username))) {
    valid = false;
    res.status(403).send({ error: 'User must be host!' });
  }
  if(valid) {
    try {
      var existingSession = await chessInterface.sessionStart(req.params.id);
      res.status(200).send(sessionTransform(existingSession));
    }
    catch(err) {
      error = typeof err === 'string' ? err : err.message;
      valid = false;
    }
  }
  if(!valid) {
    if(!res.headersSent) { res.status(403).send({ error: error }); }
  }
};

exports.move = async (req, res) => {
  var data = req.body;
  var valid = true;
  var error = '';
  if(typeof req.params.id !== 'string') {
    valid = false;
    error = 'Session id parameter is not a string!';
  }
  if(valid && typeof data.move === 'object') {
    valid = false;
    error = 'Move parameter is not an object!';
  }
  if(valid && !(await chessInterface.authPlayer(req.params.id, req.username))) {
    valid = false;
    res.status(403).send({ error: 'User is not current player!' });
  }
  if(valid) {
    try {
      var existingSession = await chessInterface.sessionMove(req.params.id, data);
      res.status(200).send(sessionTransform(existingSession));
    }
    catch(err) {
      error = typeof err === 'string' ? err : err.message;
      valid = false;
    }
  }
  if(!valid) {
    if(!res.headersSent) { res.status(403).send({ error: error }); }
  }
};

exports.undo = async (req, res) => {
  var data = req.body;
  var valid = true;
  var error = '';
  if(typeof req.params.id !== 'string') {
    valid = false;
    error = 'Session id parameter is not a string!';
  }
  if(valid && !(await chessInterface.authPlayer(req.params.id, req.username))) {
    valid = false;
    res.status(403).send({ error: 'User is not current player!' });
  }
  if(valid) {
    try {
      var existingSession = await chessInterface.sessionUndo(req.params.id);
      res.status(200).send(sessionTransform(existingSession));
    }
    catch(err) {
      error = typeof err === 'string' ? err : err.message;
      valid = false;
    }
  }
  if(!valid) {
    if(!res.headersSent) { res.status(403).send({ error: error }); }
  }
};

exports.submit = async (req, res) => {
  var data = req.body;
  var valid = true;
  var error = '';
  if(typeof req.params.id !== 'string') {
    valid = false;
    error = 'Session id parameter is not a string!';
  }
  if(valid && !(await chessInterface.authPlayer(req.params.id, req.username))) {
    valid = false;
    res.status(403).send({ error: 'User is not current player!' });
  }
  if(valid) {
    try {
      var existingSession = await chessInterface.sessionSubmit(req.params.id);
      res.status(200).send(sessionTransform(existingSession));
    }
    catch(err) {
      error = typeof err === 'string' ? err : err.message;
      valid = false;
    }
  }
  if(!valid) {
    if(!res.headersSent) { res.status(403).send({ error: error }); }
  }
};

exports.forfeit = async (req, res) => {
  var data = req.body;
  var valid = true;
  var error = '';
  if(typeof req.params.id !== 'string') {
    valid = false;
    error = 'Session id parameter is not a string!';
  }
  if(valid && !(await chessInterface.authPlayer(req.params.id, req.username))) {
    valid = false;
    res.status(403).send({ error: 'User is not current player!' });
  }
  if(valid) {
    try {
      var existingSession = await chessInterface.sessionForfeit(req.params.id);
      res.status(200).send(sessionTransform(existingSession));
    }
    catch(err) {
      error = typeof err === 'string' ? err : err.message;
      valid = false;
    }
  }
  if(!valid) {
    if(!res.headersSent) { res.status(403).send({ error: error }); }
  }
};

exports.draw = async (req, res) => {
  var data = req.body;
  var valid = true;
  var error = '';
  if(typeof req.params.id !== 'string') {
    valid = false;
    error = 'Session id parameter is not a string!';
  }
  if(valid) {
    try {
      var existingSession = await chessInterface.getSession(req.params.id);
      if(!(await chessInterface.authPlayer(req.params.id, req.username)) && existingSession.offerDraw) {
        existingSession = await chessInterface.sessionDraw(req.params.id);
        res.status(200).send(sessionTransform(existingSession));
      }
      else if(await chessInterface.authPlayer(req.params.id, req.username)) {
        existingSession = await chessInterface.sessionOfferDraw(req.params.id);
        res.status(200).send(sessionTransform(existingSession));
      }
    }
    catch(err) {
      error = typeof err === 'string' ? err : err.message;
      valid = false;
    }
  }
  if(!valid) {
    if(!res.headersSent) { res.status(403).send({ error: error }); }
  }
};

exports.getInfo = async (req, res) => {
  try {
    var session = (await chessInterface.getSession(req.params.id));
    res.status(200).send(sessionTransform(session));
  }
  catch(err) {
    res.status(500).send({ error: typeof err === 'string' ? err : err.message });
  }
};

exports.getInfoQuery = async (req, res) => {
  try {
    if(typeof req.body !== 'object') {
      req.body = {
        query: {},
        projection: {},
        sort: {},
        limit: 100,
        skip: 0
      };
    }
    if(typeof req.body.query !== 'object') { req.body.query = {}; }
    if(typeof req.body.projection !== 'object') { req.body.projection = {}; }
    if(typeof req.body.sort !== 'object') { req.body.sort = {}; }
    if(typeof req.body.limit !== 'number') { req.body.limit = 100; }
    if(typeof req.body.skip !== 'number') { req.body.skip = 0; }
    var sessions = (await chessInterface.getSessions(req.body.query, req.body.projection, req.body.sort, req.body.limit, req.body.skip)).map(e => sessionTransform(e));
    res.status(200).send(sessions);
  }
  catch(err) {
    res.status(500).send({ error: typeof err === 'string' ? err : err.message });
  }
};
