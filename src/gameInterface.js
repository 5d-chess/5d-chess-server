const rankingInterface = require('./rankingInterface');
const logInterface = require('./logInterface');
const EloUtils = require('elo-utils');
var collections = require('./db').init();
const { v5: uuidv5 } = require('uuid');

exports.newGame = async (game) => {
  var newGame = Object.assign({}, game);
  newGame.sessionId = game.id;
  newGame.id = uuidv5(newGame.variant + newGame.format + newGame.startDate, '2cdc4423-bb23-4e3c-b379-59f4a99d4f18');
  var existingGame = await collections.games.findOne({ id: newGame.id });
  if(existingGame !== null) {
    return existingGame;
  }
  var ret = (await collections.games.insert(newGame));
  if(ret.ranked) {
    var whiteOverallRanking = await rankingInterface.getLatestRanking(ret.white, 'overall', 'overall');
    var whiteSpecificRanking = await rankingInterface.getLatestRanking(ret.white, ret.variant, ret.format);
    var blackOverallRanking = await rankingInterface.getLatestRanking(ret.black, 'overall', 'overall');
    var blackSpecificRanking = await rankingInterface.getLatestRanking(ret.black, ret.variant, ret.format);
    var eloResult = (ret.winner === 'draw' ?
      EloUtils.RESULT.TIE
    : ret.winner === 'white' ?
      EloUtils.RESULT.R1
    :
      EloUtils.RESULT.R2
    );
    var eloOverallRatings = EloUtils.elo(whiteOverallRanking.rating, blackOverallRanking.rating, eloResult);
    var eloSpecificRatings = EloUtils.elo(whiteSpecificRanking.rating, blackSpecificRanking.rating, eloResult);
    await rankingInterface.addRanking(ret.white, eloOverallRatings.R1, 'overall', 'overall', ret.id, ret.endDate);
    await rankingInterface.addRanking(ret.black, eloOverallRatings.R2, 'overall', 'overall', ret.id, ret.endDate);
    await rankingInterface.addRanking(ret.white, eloSpecificRatings.R1, ret.variant, ret.format, ret.id, ret.endDate);
    await rankingInterface.addRanking(ret.black, eloSpecificRatings.R2, ret.variant, ret.format, ret.id, ret.endDate);
  }
  logInterface.addLogEntry('NewGame', 'Info', ret);
  return ret;
};

exports.getGame = async (id) => {
  var existingGame = await collections.games.findOne({ id: id });
  if(existingGame !== null) {
    return existingGame;
  }
  else {
    throw 'Game not found!';
  }
};

exports.getGames = async (query = {}, projection = {}, sort = {}, limit = 100, skip = 0) => {
  var res = (await (collections.games.findAsCursor(query, projection).sort(sort).limit(limit).skip(skip)));
  if(!Array.isArray(res)) {
    res = await res.toArray();
  }
  return res;
};
