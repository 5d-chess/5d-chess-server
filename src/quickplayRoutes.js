const quickplayInterface = require('./quickplayInterface');
const rankedFuncs = require('./ranked');

const queueTicketTransform = (queueTicket) => {
  return {
    date: queueTicket.date,
    sessionId: queueTicket.sessionId,
  };
};

exports.queue = async (req, res) => {
  let data = req.body;
  let valid = true;
  let error = '';
  if(typeof data.ranked !== 'boolean') {
    error = 'Ranked field is not boolean!';
    valid = false;
  }
  if(valid && !Array.isArray(data.variants)) {
    error = 'Variants field is not array!';
    valid = false;
  }
  if(valid && Array.isArray(data.variants)) {
    for(let variant of data.variants) {
      if(typeof variant !== 'string') {
        error = 'Variants field contains non-string element!';
        valid = false;
      }
      else if(!rankedFuncs.variants().includes(variant)) {
        error = 'Quick play must use standard variants!';
        valid = false;
      }
    }
  }
  if(valid && !Array.isArray(data.formats)) {
    error = 'Formats field is not array!';
    valid = false;
  }
  if(valid && Array.isArray(data.formats)) {
    for(let format of data.formats) {
      if(typeof format !== 'string') {
        error = 'Formats field contains non-string element!';
        valid = false;
      }
      else if(!rankedFuncs.formats().includes(format)) {
        error = 'Quick play must use standard time formats!';
        valid = false;
      }
    }
  }
  if(valid && typeof data.minRating !== 'number') {
    data.minRating = null;
  }
  if(valid && typeof data.maxRating !== 'number') {
    data.maxRating = null;
  }
  if(valid) {
    try {
      let queueToken = await quickplayInterface.queue(req.username, data.ranked, data.variants, data.formats, data.minRating, data.maxRating);
      res.status(200).send(queueTicketTransform(queueToken));
    }
    catch(err) {
      error = typeof err === 'string' ? err : err.message;
      valid = false;
    }
  }
  if(!valid) {
    if(!res.headersSent) { res.status(500).send({ error: error }); }
  }
};

exports.getInfo = async (req, res) => {
  let valid = true;
  let error = '';
  if(valid) {
    try {
      let queueToken = await quickplayInterface.getInfo(req.username);
      res.status(200).send(queueTicketTransform(queueToken));
    }
    catch(err) {
      error = typeof err === 'string' ? err : err.message;
      valid = false;
    }
  }
  if(!valid) {
    if(!res.headersSent) { res.status(500).send({ error: error }); }
  }
};

exports.cancel = async (req, res) => {
  let valid = true;
  let error = '';
  if(valid) {
    try {
      await quickplayInterface.cancel(req.username);
      res.status(200).end();
    }
    catch(err) {
      error = typeof err === 'string' ? err : err.message;
      valid = false;
    }
  }
  if(!valid) {
    if(!res.headersSent) { res.status(500).send({ error: error }); }
  }
};

exports.confirm = async (req, res) => {
  let valid = true;
  let error = '';
  if(valid) {
    try {
      await quickplayInterface.confirm(req.username);
      res.status(200).end();
    }
    catch(err) {
      error = typeof err === 'string' ? err : err.message;
      valid = false;
    }
  }
  if(!valid) {
    if(!res.headersSent) { res.status(500).send({ error: error }); }
  }
};

exports.getStats = async (req, res) => {
  let valid = true;
  let error = '';
  if(valid) {
    try {
      res.status(200).send(await quickplayInterface.getStats());
    }
    catch(err) {
      error = typeof err === 'string' ? err : err.message;
      valid = false;
    }
  }
  if(!valid) {
    if(!res.headersSent) { res.status(500).send({ error: error }); }
  }
}