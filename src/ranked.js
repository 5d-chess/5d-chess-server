const ChessClock = require('5d-chess-clock');
const Chess = require('5d-chess-js');

exports.validRanked = (variant, format) => {
  if(typeof variant !== 'string') {
    throw 'Ranked play must have a variant!';
  }
  if(typeof format !== 'string') {
    throw 'Ranked play must use time controls!';
  }
  //Check if valid variant
  let chess = new Chess();
  validVariants = chess.variants;
  let valid = false;
  for(let validVariant of validVariants) {
    if(variant === validVariant.shortName) {
      valid = true;
    }
  }
  if(!valid) {
    throw 'Ranked play must use standard variants!';
  }

  //Check if valid format
  let chessClock = new ChessClock();
  chessClock.reset(format);
  formatString = chessClock.format;
  validFormats = chessClock.formats;
  valid = false;
  for(let validFormat of validFormats) {
    if(formatString === validFormat.shortName) {
      valid = true;
    }
  }
  if(!valid) {
    throw 'Ranked play must use standard time formats!';
  }
  return true;
}

exports.variants = () => {
  let chess = new Chess();
  return chess.variants.map(e => e.shortName).filter(e => e !== 'custom');
}

exports.availableVariants = (req, res) => {
  res.status(200).send(this.variants());
}

exports.formats = () => {
  let chessClock = new ChessClock();
  return chessClock.formats.map(e => e.shortName);
}

exports.availableFormats = (req, res) => {
  res.status(200).send(this.formats());
}